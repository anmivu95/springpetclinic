FROM openjdk:11
VOLUME /main-app
ADD pet-clinic-web/build/libs/pet-clinic-web.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/app.jar"]