package fi.vincit.antti.springframework.pet_clinic.services.springdatajpa;

import fi.vincit.antti.springframework.pet_clinic.model.Pet;
import fi.vincit.antti.springframework.pet_clinic.repositories.PetRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;


@ExtendWith(MockitoExtension.class)
class PetServiceTest {

    @Mock
    private PetRepository petRepository;

    @InjectMocks
    PetSDJpaService petSDJpaService;

    private Pet returnPet;

    @BeforeEach
    void setUp(){
        returnPet = Pet.builder().id(1L).name("Max").build();
    }



    @Test
    void findAll(){
        List<Pet> returnPetsSet = new ArrayList<>();
        returnPetsSet.add(Pet.builder().name("Max").build());
        returnPetsSet.add(Pet.builder().name("Odin").build());

        when(petRepository.findAll()).thenReturn(returnPetsSet);

        Set<Pet> pets = petSDJpaService.findAll();
        assertNotNull(pets);
        assertEquals(2, pets.size());
        verify(petRepository, times(1)).findAll();

    }

    @Test
    void findById(){
        when(petRepository.findById(anyLong())).thenReturn(Optional.of(returnPet));

        Pet pet = petSDJpaService.findById(1L);
        assertNotNull(pet);
        verify(petRepository, times(1)).findById(anyLong());
    }

    @Test
    void findByIdNotFound(){
        when(petRepository.findById(anyLong())).thenReturn(Optional.empty());

        Pet pet = petSDJpaService.findById(1L);

        assertNull(pet);
    }

    @Test
    void save(){
        Pet ownerToSave = Pet.builder().id(1L).build();

        when(petRepository.save(any())).thenReturn(returnPet);
        Pet pet = petSDJpaService.save(ownerToSave);
        assertNotNull(pet);
        verify(petRepository, times(1)).save(any());
    }

    @Test
    void delete(){
        petSDJpaService.delete(returnPet);

        verify(petRepository, times(1)).delete(any());
    }

    @Test
    void deleteById(){
        petSDJpaService.deleteById(1L);
        verify(petRepository,times(1)).deleteById(anyLong());
    }


}