package fi.vincit.antti.springframework.pet_clinic.services.springdatajpa;

import fi.vincit.antti.springframework.pet_clinic.model.Owner;
import fi.vincit.antti.springframework.pet_clinic.repositories.OwnerRepository;
import fi.vincit.antti.springframework.pet_clinic.repositories.PetRepository;
import fi.vincit.antti.springframework.pet_clinic.repositories.PetTypeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class OwnerSDJpaServiceTest {
    @Mock
    private OwnerRepository ownerRepository;
    @Mock
    private PetRepository petRepository;
    @Mock
    private  PetTypeRepository petTypeRepository;

    @InjectMocks
    OwnerSDJpaService ownerSDJpaService;


    private String LAST_NAME = "Smith";
    private Owner returnOwner;

    @BeforeEach
    void setUp(){
        returnOwner = Owner.builder().id(1l).lastName("Smith").build();
    }

    @Test
    void findByLastName() {
        when(ownerRepository.findByLastName(any())).thenReturn(returnOwner);
        Owner smith = ownerSDJpaService.findByLastName(LAST_NAME);
        assertEquals("Smith",smith.getLastName());
        verify(ownerRepository, times(1)).findByLastName(any());
    }

    @Test
    void findAll(){
        List<Owner> returOwnersSet = new ArrayList<>();
        returOwnersSet.add(Owner.builder().id(1l).build());
        returOwnersSet.add(Owner.builder().id(2l).build());

        when(ownerRepository.findAll()).thenReturn(returOwnersSet);

        Set<Owner> owners = ownerSDJpaService.findAll();
        assertNotNull(owners);
        assertEquals(2, owners.size());
        verify(ownerRepository, times(1)).findAll();

    }

    @Test
    void findById(){
        when(ownerRepository.findById(anyLong())).thenReturn(Optional.of(returnOwner));

        Owner owner = ownerSDJpaService.findById(1L);
        assertNotNull(owner);
        verify(ownerRepository, times(1)).findById(anyLong());
    }

    @Test
    void findByIdNotFound(){
        when(ownerRepository.findById(anyLong())).thenReturn(Optional.empty());

        Owner owner = ownerSDJpaService.findById(1l);

        assertNull(owner);
    }

    @Test
    void save(){
        Owner ownerToSave = Owner.builder().id(1l).build();

        when(ownerRepository.save(any())).thenReturn(returnOwner);
        Owner owner = ownerSDJpaService.save(ownerToSave);
        assertNotNull(owner);
        verify(ownerRepository, times(1)).save(any());
    }

    @Test
    void delete(){
        ownerSDJpaService.delete(returnOwner);

        verify(ownerRepository, times(1)).delete(any());
    }

    @Test
    void deleById(){
        ownerSDJpaService.deleteById(1L);
        verify(ownerRepository,times(1)).deleteById(anyLong());
    }

}