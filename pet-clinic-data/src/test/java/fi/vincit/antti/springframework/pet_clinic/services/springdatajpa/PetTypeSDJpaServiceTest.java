package fi.vincit.antti.springframework.pet_clinic.services.springdatajpa;

import fi.vincit.antti.springframework.pet_clinic.model.PetType;
import fi.vincit.antti.springframework.pet_clinic.repositories.PetTypeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PetTypeSDJpaServiceTest {


    @Mock
    private PetTypeRepository petTypeRepository;

    @InjectMocks
    PetTypeSDJpaService petTypeSDJpaService;

    private PetType returnPetType;

    @BeforeEach
    void setUp(){
        returnPetType = PetType.builder().id(1L).name("Husky").build();
    }


    @Test
    void findAll(){
        List<PetType> returnPetTypesSet = new ArrayList<>();
        returnPetTypesSet.add(PetType.builder().name("husky").build());
        returnPetTypesSet.add(PetType.builder().name("shepper").build());

        when(petTypeRepository.findAll()).thenReturn(returnPetTypesSet );

        Set<PetType> petTypes = petTypeSDJpaService.findAll();
        assertNotNull(petTypes);
        assertEquals(2, petTypes.size());
        verify(petTypeRepository, times(1)).findAll();
    }

    @Test
    void findById(){
        when(petTypeRepository.findById(anyLong())).thenReturn(Optional.of(returnPetType));

        PetType petType = petTypeSDJpaService.findById(1L);
        assertNotNull(petType);
        verify(petTypeRepository, times(1)).findById(anyLong());
    }

    @Test
    void findByIdNotFound(){
        when(petTypeRepository.findById(anyLong())).thenReturn(Optional.empty());

        PetType petType = petTypeSDJpaService.findById(1L);

        assertNull(petType);
    }

    @Test
    void save(){
        PetType petTypeSave = PetType.builder().id(1L).build();

        when(petTypeRepository.save(any())).thenReturn(returnPetType);
        PetType petType = petTypeSDJpaService.save(petTypeSave);
        assertNotNull(petType);
        verify(petTypeRepository, times(1)).save(any());
    }

    @Test
    void delete(){
        petTypeSDJpaService.delete(returnPetType);

        verify(petTypeRepository, times(1)).delete(any());
    }

    @Test
    void deleteById(){
        petTypeSDJpaService.deleteById(1L);
        verify(petTypeRepository,times(1)).deleteById(anyLong());
    }

}