package fi.vincit.antti.springframework.pet_clinic.services;

import fi.vincit.antti.springframework.pet_clinic.model.Visit;

public interface VisitService extends CrudService<Visit, Long> {
}
