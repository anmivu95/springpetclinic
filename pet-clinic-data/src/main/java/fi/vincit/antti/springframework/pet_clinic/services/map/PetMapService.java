package fi.vincit.antti.springframework.pet_clinic.services.map;

import fi.vincit.antti.springframework.pet_clinic.model.Pet;
import fi.vincit.antti.springframework.pet_clinic.services.PetService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;


@Service
@Profile({"default","map"})
public class PetMapService extends  AbstractMapService<Pet, Long> implements PetService {


}
