package fi.vincit.antti.springframework.pet_clinic.services.springdatajpa;

import fi.vincit.antti.springframework.pet_clinic.model.Pet;
import fi.vincit.antti.springframework.pet_clinic.repositories.PetRepository;
import fi.vincit.antti.springframework.pet_clinic.services.PetService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;



@Service
@Profile({"springdatajpa", "prod"})
public class PetSDJpaService extends AbstractJpaService<Pet, PetRepository> implements PetService {


    public PetSDJpaService(PetRepository petRepository) {
         super(petRepository);
    }

}
