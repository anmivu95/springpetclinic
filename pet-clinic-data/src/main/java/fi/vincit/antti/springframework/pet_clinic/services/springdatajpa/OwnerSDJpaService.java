package fi.vincit.antti.springframework.pet_clinic.services.springdatajpa;

import fi.vincit.antti.springframework.pet_clinic.model.Owner;
import fi.vincit.antti.springframework.pet_clinic.repositories.OwnerRepository;
import fi.vincit.antti.springframework.pet_clinic.services.OwnerService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@Profile({"springdatajpa", "prod"})
public class OwnerSDJpaService extends AbstractJpaService<Owner, OwnerRepository> implements OwnerService {



    public OwnerSDJpaService(OwnerRepository ownerRepository) {
        super(ownerRepository);

    }

    @Override
    public Owner findByLastName(String lastName) {
        return repository.findByLastName(lastName);
    }

    @Override
    public List<Owner> findAllByLastNameLikeIgnoreCase(String lastName) {
        return repository.findAllByLastNameLikeIgnoreCase(lastName);
    }
}
