package fi.vincit.antti.springframework.pet_clinic.services;


import fi.vincit.antti.springframework.pet_clinic.model.Vet;


public interface VetService extends CrudService<Vet, Long> {
}
