package fi.vincit.antti.springframework.pet_clinic.services;

import fi.vincit.antti.springframework.pet_clinic.model.Owner;

import java.util.List;


public interface OwnerService extends CrudService<Owner, Long>  {

    Owner findByLastName(String lastName);

    List<Owner> findAllByLastNameLikeIgnoreCase(String lastName);


}
