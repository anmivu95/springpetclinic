package fi.vincit.antti.springframework.pet_clinic.services;

import fi.vincit.antti.springframework.pet_clinic.model.PetType;

public interface PetTypeService  extends  CrudService<PetType, Long>{
}
