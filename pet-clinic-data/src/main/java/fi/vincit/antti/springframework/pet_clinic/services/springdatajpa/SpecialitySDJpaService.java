package fi.vincit.antti.springframework.pet_clinic.services.springdatajpa;

import fi.vincit.antti.springframework.pet_clinic.model.Speciality;
import fi.vincit.antti.springframework.pet_clinic.repositories.SpecialityRepository;
import fi.vincit.antti.springframework.pet_clinic.services.SpecialtyService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;


@Service
@Profile({"springdatajpa", "prod"})
public class SpecialitySDJpaService extends AbstractJpaService<Speciality, SpecialityRepository> implements SpecialtyService {

    public SpecialitySDJpaService(SpecialityRepository specialityRepository) {
        super(specialityRepository);
    }
}
