package fi.vincit.antti.springframework.pet_clinic.services.springdatajpa;

import fi.vincit.antti.springframework.pet_clinic.model.Vet;
import fi.vincit.antti.springframework.pet_clinic.repositories.VetRepository;
import fi.vincit.antti.springframework.pet_clinic.services.VetService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;


@Service
@Profile({"springdatajpa", "prod"})
public class VetSDJpaService extends AbstractJpaService<Vet, VetRepository> implements VetService {

    public VetSDJpaService(VetRepository vetRepository) {
        super(vetRepository);
    }
}
