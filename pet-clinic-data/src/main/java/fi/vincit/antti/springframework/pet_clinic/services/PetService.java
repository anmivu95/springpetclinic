package fi.vincit.antti.springframework.pet_clinic.services;

import fi.vincit.antti.springframework.pet_clinic.model.Pet;

public interface PetService extends CrudService<Pet, Long> { }
