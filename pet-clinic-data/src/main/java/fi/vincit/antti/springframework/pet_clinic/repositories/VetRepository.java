package fi.vincit.antti.springframework.pet_clinic.repositories;

import fi.vincit.antti.springframework.pet_clinic.model.Vet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface  VetRepository extends JpaRepository<Vet, Long> {
}
