package fi.vincit.antti.springframework.pet_clinic.services.map;

import fi.vincit.antti.springframework.pet_clinic.model.Speciality;
import fi.vincit.antti.springframework.pet_clinic.model.Vet;
import fi.vincit.antti.springframework.pet_clinic.services.SpecialtyService;
import fi.vincit.antti.springframework.pet_clinic.services.VetService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;



@Service
@Profile({"default","map"})
public class VetMapService extends  AbstractMapService<Vet, Long> implements VetService {
    private final SpecialtyService specialtyService;

    public VetMapService(SpecialtyService specialtyService) {
        this.specialtyService = specialtyService;
    }

    @Override
    public Vet save(Vet object) {
        if (!object.getSpecialities().isEmpty()){
            object.getSpecialities().forEach(specialty -> {
                if (specialty.getId() == null){
                    Speciality savedSpeciality = specialtyService.save(specialty);
                    specialty.setId(savedSpeciality.getId());
                }
            });

        }
        return super.save(object);
    }


}
