package fi.vincit.antti.springframework.pet_clinic.repositories;

import fi.vincit.antti.springframework.pet_clinic.model.PetType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PetTypeRepository extends JpaRepository<PetType, Long> {
}
