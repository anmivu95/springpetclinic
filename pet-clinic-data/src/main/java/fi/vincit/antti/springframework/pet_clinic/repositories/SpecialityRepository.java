package fi.vincit.antti.springframework.pet_clinic.repositories;

import fi.vincit.antti.springframework.pet_clinic.model.Speciality;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SpecialityRepository extends JpaRepository<Speciality, Long> {
}
