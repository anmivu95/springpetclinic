package fi.vincit.antti.springframework.pet_clinic.services.map;

import fi.vincit.antti.springframework.pet_clinic.model.Visit;
import fi.vincit.antti.springframework.pet_clinic.services.VisitService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;


@Service
@Profile({"default","map"})
public class VisitMapService extends  AbstractMapService<Visit, Long> implements VisitService {



    @Override
    public Visit save(Visit visit) {

        if(visit.getPet() == null || visit.getPet().getOwner() == null || visit.getPet().getId() == null
                || visit.getPet().getOwner().getId() == null){
            throw new RuntimeException("Invalid visit on save");
        }
        return super.save(visit);
    }


}
