package fi.vincit.antti.springframework.pet_clinic.services.springdatajpa;

import fi.vincit.antti.springframework.pet_clinic.model.Visit;
import fi.vincit.antti.springframework.pet_clinic.repositories.VisitRepository;
import fi.vincit.antti.springframework.pet_clinic.services.VisitService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;


@Service
@Profile({"springdatajpa", "prod"})
public class VisitSDJpaService extends AbstractJpaService<Visit, VisitRepository> implements VisitService {

    public VisitSDJpaService(VisitRepository visitRepository) {
         super(visitRepository);
    }
}
