package fi.vincit.antti.springframework.pet_clinic.services.map;

import fi.vincit.antti.springframework.pet_clinic.model.Speciality;
import fi.vincit.antti.springframework.pet_clinic.services.SpecialtyService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;



@Service
@Profile({"default","map"})
public class SpecialtyMapService extends AbstractMapService<Speciality, Long> implements SpecialtyService {

}
