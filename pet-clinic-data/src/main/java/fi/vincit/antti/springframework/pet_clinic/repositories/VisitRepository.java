package fi.vincit.antti.springframework.pet_clinic.repositories;

import fi.vincit.antti.springframework.pet_clinic.model.Visit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VisitRepository extends JpaRepository<Visit, Long> {
}
