package fi.vincit.antti.springframework.pet_clinic.repositories;

import fi.vincit.antti.springframework.pet_clinic.model.Pet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PetRepository extends JpaRepository<Pet, Long> {
}
