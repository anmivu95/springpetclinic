package fi.vincit.antti.springframework.pet_clinic.services.springdatajpa;

import fi.vincit.antti.springframework.pet_clinic.model.PetType;
import fi.vincit.antti.springframework.pet_clinic.repositories.PetTypeRepository;
import fi.vincit.antti.springframework.pet_clinic.services.PetTypeService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile({"springdatajpa", "prod"})
public class PetTypeSDJpaService extends AbstractJpaService<PetType, PetTypeRepository> implements PetTypeService {



    public PetTypeSDJpaService(PetTypeRepository petTypeRepository) {
        super(petTypeRepository);
    }

}
