package fi.vincit.antti.springframework.pet_clinic.services.map;

import fi.vincit.antti.springframework.pet_clinic.model.Owner;
import fi.vincit.antti.springframework.pet_clinic.model.Pet;
import fi.vincit.antti.springframework.pet_clinic.services.OwnerService;
import fi.vincit.antti.springframework.pet_clinic.services.PetService;
import fi.vincit.antti.springframework.pet_clinic.services.PetTypeService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;


@Service
@Profile({"default","map"})
public class OwnerMapService extends  AbstractMapService<Owner, Long> implements OwnerService {

    private  final PetTypeService petTypeService;
    private  final PetService petService;

    public OwnerMapService(PetTypeService petTypeService, PetService petService) {
        this.petTypeService = petTypeService;
        this.petService = petService;
    }

    @Override
    public Owner save(Owner object) {

        if (object != null){
            if (object.getPets() != null){
                object.getPets().forEach(this::savePet);
            }
            return  super.save(object);
        }else {
            return  null;
        }

    }

    private void savePet(Pet pet){
        if (pet.getPetType() != null){
            if (pet.getPetType().getId() == null){
                pet.setPetType(petTypeService.save(pet.getPetType()));
            }
        }else{
            throw  new RuntimeException("Pet Type is Required");
        }
        if (pet.getId() == null){
            Pet savedPet = petService.save(pet);
            pet.setId(savedPet.getId());
        }
    }



    @Override
    public Owner findByLastName(String lastName) {
        return findAll().stream().filter(owner -> owner.getLastName().equalsIgnoreCase(lastName)).findFirst().orElse(null);
    }

    @Override
    public List<Owner> findAllByLastNameLikeIgnoreCase(String name) {
        return Collections.emptyList();
    }
}
