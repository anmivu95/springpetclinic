package fi.vincit.antti.springframework.pet_clinic.services;

import fi.vincit.antti.springframework.pet_clinic.model.Speciality;

public interface SpecialtyService extends CrudService <Speciality, Long>  {
}
