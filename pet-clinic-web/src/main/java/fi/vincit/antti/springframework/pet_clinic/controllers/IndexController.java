package fi.vincit.antti.springframework.pet_clinic.controllers;


import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Slf4j
@Controller
public class IndexController {


    @RequestMapping({"","/","index","index.html"})
    public String index(){
        log.debug("Index Controller");
        return "index";
    }

    @RequestMapping("/oups")
    public String oupsHandler(){
        return "notimplemented";
    }
}
