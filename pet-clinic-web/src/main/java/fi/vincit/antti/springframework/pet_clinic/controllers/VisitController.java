package fi.vincit.antti.springframework.pet_clinic.controllers;


import fi.vincit.antti.springframework.pet_clinic.model.Pet;
import fi.vincit.antti.springframework.pet_clinic.model.Visit;
import fi.vincit.antti.springframework.pet_clinic.services.PetService;
import fi.vincit.antti.springframework.pet_clinic.services.VisitService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@Slf4j
@Controller
public class VisitController {
    private final VisitService visitService;
    private final PetService petService;

    public VisitController(VisitService visitService, PetService petService) {
        this.visitService = visitService;
        this.petService = petService;
    }

    @InitBinder
    public void setAllowedFields(WebDataBinder dataBinder) {
        dataBinder.setAllowedFields("id");
    }


    /**
     * Called before each @RequestMapping
     * Created fresh data for the calls.
     * @param petId
     * @return Pet
     */
    @ModelAttribute("visit")
    public Visit loadPetWithVisit(@PathVariable("petId") long  petId, Map<String, Object> model) {
        Pet pet = petService.findById(petId);
        model.put("pet", pet);
        Visit visit = new Visit();
        pet.getVisits().add(visit);
        visit.setPet(pet);
        return visit;
    }

    //Spring MVC calls method loadPetWithVisits(...) before method is called
    @GetMapping("/owners/*/pets/{petId}/visits/new")
    public String initNewVisitForm(@PathVariable("petId") long petId, Map<String, Object> model){
        return "pets/createOrUpdateVisitForm";
    }

    //Spring MVC calls method loadPetWithVisits(...) before method is called
    @PostMapping("/owners/{ownerId}/pets/{petId}/visits/new")
            public String processNewVisitForm(@Valid Visit visit, BindingResult result){
        if (result.hasErrors()){
            return "pets/createOrUpdateVisitForm";
        } else {
            visitService.save(visit);
            return "redirect:/owners/{ownerId}";
        }
    }


}
