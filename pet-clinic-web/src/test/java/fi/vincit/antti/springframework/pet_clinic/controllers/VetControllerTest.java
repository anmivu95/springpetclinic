package fi.vincit.antti.springframework.pet_clinic.controllers;

import fi.vincit.antti.springframework.pet_clinic.model.Owner;
import fi.vincit.antti.springframework.pet_clinic.model.Vet;
import fi.vincit.antti.springframework.pet_clinic.services.VetService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@ExtendWith(MockitoExtension.class)
class VetControllerTest {


    @Mock
    VetService vetService;

    @InjectMocks
    VetController vetController;

    Set<Vet> vets;

    MockMvc mockMvc;


    @BeforeEach
    void setUp(){
        vets = new HashSet<>();
        vets.add(Vet.builder().id(1l).build());
        vets.add(Vet.builder().id(2l).build());
        mockMvc = MockMvcBuilders.standaloneSetup(vetController).build();
    }

    @Test
    void listOwners() throws Exception {
        when(vetService.findAll()).thenReturn(vets);

        mockMvc.perform(get("/vets")).andExpect(status().isOk())
                .andExpect(view().name("vets/index"))
                .andExpect(model().attribute("vets", hasSize(2)));
    }

    @Test
    void listOwnersByIndex() throws Exception {
        when(vetService.findAll()).thenReturn(vets);

        mockMvc.perform(get("/vets/index")).andExpect(status().isOk())
                .andExpect(view().name("vets/index"))
                .andExpect(model().attribute("vets", hasSize(2)));
    }

    @Test
    void findOwners() throws Exception {
        mockMvc.perform(get("/vets/find")).andExpect(status().isOk())
                .andExpect(view().name("notimplemented"));
        verifyNoInteractions(vetService);
    }


}